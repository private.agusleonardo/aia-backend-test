const express = require("express");
const app = express();
const pool = require("./db");
const bodyParser = require("body-parser");
const request = require("request");

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

var jsonTemp = "";

app.get("/", (req, res) => {
  res.status(200).json({
    message: "Only served as app server",
  });
});

app.get("/user", (req, res) => {
  pool.query(`SELECT * FROM public.user`, (error, response) => {
    var rows = response.rows;

    res.status(200).json({
      message: "Login success",
      data: `rows: ${rows[0]} response: ${response}`,
    });
  });
});

app.post("/login", async (req, res) => {
  try {
    const { username, password } = req.body;
    const client = await pool.connect();

    client.query(
      `SELECT name, username, password FROM public.user WHERE username = '${username.toUpperCase()}'`,
      (error, response) => {
        var rows = response.rows;

        if (rows.length > 0) {
          if (password === rows[0].password) {
            res.status(200).json({
              message: "Login success",
              data: { user: { username: rows[0].name } },
            });
          } else {
            res.status(422).json({ message: "Wrong password" });
          }
        } else {
          res.status(422).json({ message: "Username not found" });
        }
      }
    );
    client.release();
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ message: error.message });
  }
});

app.post("/user/add", async (req, res) => {
  try {
    const { name, username, password } = req.body;
    const newUser = await pool.query(
      `INSERT INTO public.user (name, username, password) VALUES ('${name}', '${username.toUpperCase()}', '${password}')`
    );
    res.status(200).json({ message: "User is added" });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ message: error.message });
  }
});

app.post("/picture", (req, res) => {
  try {
    var tags = req.body.tags;
    var page = req.body.page;

    console.log(page);

    var link = `https://www.flickr.com/services/feeds/photos_public.gne?format=json&tags=${tags}`.toString();

    if (jsonTemp === undefined || page === "1") {
      request(link, function (error, response, body) {
        if (!error && response.statusCode == 200) {
          var jsonString = response.body.substring(15).slice(0, -1);
          jsonTemp = jsonString;
          var json = JSON.parse(jsonString);
          json.items = json.items.splice(0, 10);
          res.status(200).json(json);
        }
      });
    } else {
      var json = JSON.parse(jsonTemp);
      json.items = json.items.splice(10, 10);
      res.status(200).json(json);
    }
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ message: error.message });
  }
});

const PORT = process.env.PORT || 80;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
